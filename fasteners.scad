module screw(l,d,headD,cs=false){
    translate([0,0,-l+d]){
        cylinder(d=d,h=l);
        if(cs==true){
            cylinder(d1=headD,d2=0,h=headD/2);
        } 
        translate([0,0,-t])
            cylinder(d=headD,h=t);
    }
}

module screwM4x10CS(){
    screw(l=10,d=4,headD=7.7,cs=true);
}

module screwWood4x10(){
    screw(l=8,d=4,headD=8);
}
