module trapezoid(w1, w2, h, w, h1, h2){
    if(w1!=undef && w2!=undef && h!=undef){
        polygon([
            [w1/2, h],
            [w2/2, 0],
            [-w2/2, 0],
            [-w1/2, h]
        ]);
    } else if(h1!=undef && h2!=undef && w!=undef){
        polygon([
            [0, -h1/2],
            [w, -h2/2],
            [w, h2/2],
            [0, h1/2]
        ]);
    }
}

module arc(w,s,r){
    l=w/2;
    r=r==undef ? arcRFromLS(l=l,s=s) : r;
    s=s==undef ? arcSFromRL(r=r,l=l) : s;
    //echo(l,s,r);
    difference(){
        translate([0,s-r])
            circle(r=r);
        translate([0,-r])
            square([2*r,2*r],center=true);
    }
}

module slice(a,r){
    module qtSlice(){
        polygon([
            [0,0],
            [2*r,0],
            [2*r,2*r],
            [cos(a/4)*2*r,sin(a/4)*2*r]
        ]);
    }
    
    difference(){
        circle(r=r);
        difference(){
            square([2*r,2*r],center=true);
            for(rt=[0:3])
                rotate(a=[0,0,(a/4)*rt])
                    qtSlice();
        }
    }
}

module radius(r){
    offset(r=r) offset(delta=-r)
        children();
}

module fillet(r,a){
	rotate(a=a)
		difference(){
			square(size=r);
			translate([r,r])
				circle(r=r);
		}
}

module jrotate(a,v,o){
    //o=offset, passed to translates
    translate(o)
        rotate(a=a,v=v)
            translate(o*-1)
                children();
}

function arcSFromRL(r,l)=r-sqrt(pow(r,2)-pow(l,2));
function arcRFromLS(l,s)=(pow(s,2)+pow(l,2))/(2*s);